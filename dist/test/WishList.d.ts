/**
 * @define WishList 心愿表
 * @static {string} PRIMARY_KEY 主键名称
 * @property {string} id 主键
 * @property {string} title 名称
 * @property {string} category 种类
 * @property {number} price 价格
 * @property {string} pic 图片
 * @property {string} belong_to 归属于
 */
import GSGY from '../src';
export default class WishList extends GSGY.Database.SQLModel {
    primary_key: string;
    table: string;
    columns: {
        id: string | undefined;
        title: string | undefined;
        category: string | undefined;
        price: string | undefined;
        pic: string | undefined;
        belong_to: string | undefined;
        taobao_code: string | undefined;
    };
}
//# sourceMappingURL=WishList.d.ts.map