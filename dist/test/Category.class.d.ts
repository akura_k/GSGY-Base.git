import GSGY from '../src/index';
export default class Category extends GSGY.Database.SQLModel {
    static IS_DELETE: {
        DELETED: number;
        NOT_DELETED: number;
    };
    static table: string;
    static primaryKey: string;
    id: String | null;
    pid: String | null;
    name: String | null;
    seq: Number | null;
    is_delete: String | null;
}
//# sourceMappingURL=Category.class.d.ts.map