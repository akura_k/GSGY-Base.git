"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
/**
 * @define WishList 心愿表
 * @static {string} PRIMARY_KEY 主键名称
 * @property {string} id 主键
 * @property {string} title 名称
 * @property {string} category 种类
 * @property {number} price 价格
 * @property {string} pic 图片
 * @property {string} belong_to 归属于
 */
var src_1 = __importDefault(require("../src"));
var WishList = /** @class */ (function (_super) {
    __extends(WishList, _super);
    function WishList() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.primary_key = 'id';
        _this.table = 'wish_list';
        _this.columns = {
            id: undefined,
            title: undefined,
            category: undefined,
            price: undefined,
            pic: undefined,
            belong_to: undefined,
            taobao_code: undefined
        };
        return _this;
    }
    return WishList;
}(src_1.default.Database.SQLModel));
exports.default = WishList;
//# sourceMappingURL=WishList.js.map