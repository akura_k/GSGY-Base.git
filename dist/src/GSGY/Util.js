"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var uuid_1 = require("uuid");
var Util = /** @class */ (function () {
    function Util() {
    }
    /**
     * 生成UUID（唯一ID）
     */
    Util.generateUUID = function () {
        return uuid_1.v4();
    };
    return Util;
}());
exports.default = Util;
//# sourceMappingURL=Util.js.map