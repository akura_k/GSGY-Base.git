"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
/**
 * @class SQLModel 数据库模型对象
 */
var SQLModel = /** @class */ (function () {
    function SQLModel() {
    }
    /**
     * 获取表名
     */
    SQLModel.prototype.getTable = function () {
        return this.constructor.table;
    };
    /**
     * 获取主键
     */
    SQLModel.prototype.getPrimaryKey = function () {
        return this.constructor.primaryKey;
    };
    /**
     * 反序列化一个列表
     * @param results
     */
    SQLModel.deserializeList = function (results) {
        var _this = this;
        return results.map(function (row) { return _this.deserializeModel(row); });
    };
    /**
     * 反序列化一个实例
     * @param result
     */
    SQLModel.deserializeModel = function (result) {
        var model = new this;
        return model.fill(result);
    };
    /**
     * 用对象填充Model
     * @param result
     * @return {T extends SQLModel} 实例本身 支持链式调用
     */
    SQLModel.prototype.fill = function (result) {
        for (var column in this) {
            if (Object.prototype.hasOwnProperty.call(this, column)) {
                this[column] = result[column];
            }
        }
        return this;
    };
    SQLModel.table = '';
    SQLModel.primaryKey = '';
    return SQLModel;
}());
exports.default = SQLModel;
//# sourceMappingURL=SQLModel.js.map