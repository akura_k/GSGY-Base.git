/**
 * @define Message 消息通用类
 * @property CONST 常量
 * @property status 响应状态
 * @property errCode 错误编号
 * @property message 提示信息
 * @property content 响应内容
 */
export default class Message {
    static STATUS: {
        SUCCESS: string;
        FAIL: string;
        UNKNOWN: string;
    };
    static ERROR_CODE: {
        UNKNOWN: string;
        NO_ERROR: string;
    };
    status: string;
    errCode: string;
    message: string;
    content: any;
}
//# sourceMappingURL=Message.d.ts.map