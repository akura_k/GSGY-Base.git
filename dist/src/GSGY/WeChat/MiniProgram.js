"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var axios_1 = __importDefault(require("axios"));
var Message_1 = __importDefault(require("../Model/Message"));
var API_PATH = "https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential";
var MiniProgram = /** @class */ (function () {
    function MiniProgram(APPID, APPSECRET) {
        this.APPID = '';
        this.APPSECRET = '';
        this.APPID = APPID;
        this.APPSECRET = APPSECRET;
    }
    MiniProgram.prototype.getAccessToken = function () {
        var url = API_PATH + "&appid=" + this.APPID + "&secret=" + this.APPSECRET;
        axios_1.default.get(url)
            .then(function (_a) {
            var _b = _a.data, errcode = _b.errcode, errmsg = _b.errmsg, access_token = _b.access_token, expires_in = _b.expires_in;
            var message = new Message_1.default();
            message.status = Message_1.default.STATUS.FAIL;
            message.errCode = errcode;
            message.message = errmsg;
            switch (errcode) {
                case '-1':
                    // 系统繁忙，此时请开发者稍候再试
                    message.message += '/系统繁忙，此时请开发者稍候再试';
                    break;
                case '40001':
                    // AppSecret 错误或者 AppSecret 不属于这个小程序，请开发者确认 AppSecret 的正确性
                    message.message += '/AppSecret 错误或者 AppSecret 不属于这个小程序，请开发者确认 AppSecret 的正确性';
                    break;
                case '40002':
                    // 请确保 grant_type 字段值为 client_credential
                    message.message += '/请确保 grant_type 字段值为 client_credential';
                    break;
                case '40013':
                    // 不合法的 AppID，请开发者检查 AppID 的正确性，避免异常字符，注意大小写
                    message.message += '/不合法的 AppID，请开发者检查 AppID 的正确性，避免异常字符，注意大小写';
                    break;
                case '0':
                default:
                    message.errCode = Message_1.default.ERROR_CODE.NO_ERROR;
                    message.status = Message_1.default.STATUS.SUCCESS;
                    message.content = access_token;
                    // 成功
                    break;
            }
        });
    };
    return MiniProgram;
}());
exports.default = MiniProgram;
//# sourceMappingURL=MiniProgram.js.map