"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var fs_1 = __importDefault(require("fs"));
var path_1 = __importDefault(require("path"));
var Config_1 = __importDefault(require("./Config"));
var lodash_1 = __importDefault(require("lodash"));
require("datejs");
/**
 * 日志模块
 */
var Log = /** @class */ (function () {
    function Log() {
    }
    Log.write = function (obj, errMsg) {
        var log = new Date().toString('yyyy-MM-dd HH:mm:ss') + "\n";
        if (lodash_1.default.isError(obj)) {
            console.error(obj);
            log += "\u53D1\u751F\u9519\u8BEF\uFF01\u9519\u8BEF\u4FE1\u606F\uFF1A" + errMsg + "\n";
            log += "\u9519\u8BEF\u6808\uFF1A" + obj.stack;
        }
        else if (typeof obj == 'string') {
            log += "" + obj;
        }
        else {
            log += "" + JSON.stringify(obj);
        }
        console.log(log);
        fs_1.default.mkdirSync(Log.logPath, { recursive: true });
        fs_1.default.writeFileSync(path_1.default.resolve(Log.logPath, new Date().toString('yyyyMMdd') + '.log'), log + '\n', {
            flag: 'a'
        });
    };
    var _a;
    /**
     * 日志路径
     * @desc 从配置文件取，如果配置文件中没有，则默认在项目根目录/log下
     */
    Log.logPath = Config_1.default.get('log') ||
        path_1.default.resolve(path_1.default.resolve(path_1.default.dirname(((_a = require.main) === null || _a === void 0 ? void 0 : _a.filename) || ''), './logs'));
    return Log;
}());
exports.default = Log;
//# sourceMappingURL=Log.js.map