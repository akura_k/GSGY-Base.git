import axios from 'axios'
import Message from '../Model/Message'

const API_PATH
    = `https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential`

export default class MiniProgram {
    APPID: string = ''
    APPSECRET: string = ''

    constructor(APPID: string, APPSECRET: string) {
        this.APPID = APPID
        this.APPSECRET = APPSECRET
    }

    getAccessToken() {
        const url = `${API_PATH}&appid=${this.APPID}&secret=${this.APPSECRET}`
        axios.get(url)
            .then(({data: {errcode, errmsg, access_token, expires_in}}) => {
                let message = new Message()
                message.status = Message.STATUS.FAIL
                message.errCode = errcode
                message.message = errmsg
                switch (errcode) {
                    case '-1':
                        // 系统繁忙，此时请开发者稍候再试
                        message.message += '/系统繁忙，此时请开发者稍候再试'
                        break
                    case '40001':
                        // AppSecret 错误或者 AppSecret 不属于这个小程序，请开发者确认 AppSecret 的正确性
                        message.message += '/AppSecret 错误或者 AppSecret 不属于这个小程序，请开发者确认 AppSecret 的正确性'
                        break
                    case '40002':
                        // 请确保 grant_type 字段值为 client_credential

                        message.message += '/请确保 grant_type 字段值为 client_credential'
                        break
                    case '40013':
                        // 不合法的 AppID，请开发者检查 AppID 的正确性，避免异常字符，注意大小写
                        message.message += '/不合法的 AppID，请开发者检查 AppID 的正确性，避免异常字符，注意大小写'
                        break
                    case '0':
                    default:
                        message.errCode = Message.ERROR_CODE.NO_ERROR
                        message.status = Message.STATUS.SUCCESS
                        message.content = access_token
                        // 成功
                        break
                }
            })
    }
}