import fs from 'fs'
import path from 'path'
import _ from 'lodash'

/// 读取配置 开始
/**
 * 读取配置
 * @desc 默认读取路径：项目根目录/gsgy.config.js
 */
let CONFIG:object
try{
    CONFIG = JSON.parse(fs.readFileSync(path.resolve(path.dirname(require.main?.filename||''), './gsgy.config.json')).toString())
}
catch (e) {
    CONFIG = {}
}
///! 读取配置 结束

/**
 * 配置模块
 */
export default class Config {
    /**
     * 读取配置
     * @param path 配置名称
     */
    static get(path: string):any {
        try {
            return _.get(CONFIG, path)
        }
        catch (e) {
            return ''
        }
    }
}
