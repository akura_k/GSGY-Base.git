import {v4 as uuidV4} from 'uuid'
export default class Util {
    /**
     * 生成UUID（唯一ID）
     */
    static generateUUID():string{
        return uuidV4()
    }
}