import _ from 'lodash'
import Message from '../Model/Message'
import SQLModel from './SQLModel'
import SQLConnection from './SQLConnection'
import Config from '../Config'


/**
 * @class MySQLClient 数据库操作类
 * @property {MySQLConnection} connection 数据库连接对象
 */
export default abstract class SQLClient {
    static CONST = {
        ERROR_CODE: {
            DELETE_NOT_FOUND: 'delete_not_found',
            SELECT_NOT_FOUND: 'select_not_found',
            INSERT_NOTHING: 'insert_nothing',
            PRIMARY_KEY_UNDEFINED:'primary_key_undefined',
            UNKNOWN: Message.ERROR_CODE.UNKNOWN
        }
    }

    connection: SQLConnection | null = null

    /**
     * @constructor 构造函数
     * @param {MySQLConnection} connection 数据库连接对象
     */
    protected constructor(connection: SQLConnection)
    /**
     * @constructor 构造函数
     * @param {string} connectionKey 配置名称
     */
    protected constructor(connectionKey: string)
    protected constructor(connection: SQLConnection | string) {
    }


    /**
     * 新增记录
     * @param {SQLModel} model
     * @return {Promise<Message>}
     */
    abstract async insert<T extends SQLModel>(model: T): Promise<Message>

    /**
     * 删除记录
     * @param {SQLModel} model
     * @return {Promise<Message>}
     */
    abstract async delete<T extends SQLModel>(model: T): Promise<Message>

    /**
     * 查询单条记录
     * @param {SQLModel} model
     * @return {Promise<Message>}
     */
    abstract async select<T extends SQLModel>(model: T): Promise<Message>

    /**
     * 查询记录分页
     * @param {SQLModel} model
     * @param pageIndex
     * @param pageSize
     * @return {Promise<Message>}
     */
    abstract async page<T extends SQLModel>(model: T, pageIndex: number, pageSize: number): Promise<Message>

    /**
     * 根据主键修改记录
     * @param {SQLModel} model
     * @return {Promise<Message>}
     */
    abstract async update<T extends SQLModel>(model: T): Promise<Message>

    /**
     * 运行SQL语句
     * @param {string} sql
     * @param {Array<string>} [params]
     * @return {Promise<Message>}
     */
    abstract async runSQL(sql: string, params?: Array<string>): Promise<Message>
}